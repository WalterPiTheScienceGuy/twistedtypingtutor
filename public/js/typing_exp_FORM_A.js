// Events of interest
// keypress, (maybe keyup)
// (myabe error)
// click                [element-specific]
// focus, or focusin    [element-specific]
// submit              
// (maybe input, change)
// (maybe DOMSubtreeModified)

var started = false
var startTime
var typedString = ''
var EXPERIMENT_COMPLETE = false;

//var E1_text = 'On many long journeys have I gone. And waited, too, for others to return from journeys of their own. Some return; some are broken; some come back so different only their names remain.'
var E1_text = "You miss 100% of the shots you don't take."
var E2_text = 'On many long journeys have I gone. And waited, too, for others to return from journeys of their own. Some return; some are broken; some come back so different only their names remain.'
var E3_text = 'How did you know, how did you know, Master Yoda? Master Yoda knows these things. His job it is.'
var E4_text = 'The more nobly and magnanimously thou conductest thyself, and the less thou vauntest of thy wealth and power.'
var E5_text = "Secret, shall I tell you? Grand Master of Jedi Order am I. Won this job in a raffle I did, think you?"

var E_texts = [E1_text, E2_text, E3_text, E4_text, E5_text] 

var switched_E1 = ['S','S'] //control
var switched_E2 = ['S','S','R','R'] //['S','O','R','A'] //Form A Switch 0
var switched_E3 = ['O','I'] //Form A Switch 1
var switched_E4 = ['E','R','T','A'] //Form A Switch 2
var switched_E5 = ['H','M','O','N','J','I'] //Form A Switch 3


var switched_E = [switched_E1, switched_E2, switched_E3, switched_E4, switched_E5]
var switch_current = true

var num_E_complete = 0

var consoleArray = new Array()
consoleArray.push('Copy this text and send to experimenter: ')

var textEntered
var lastkey
var passedTime

var token = false
var token2 = false // FIX CLEAR TEXTAREA

function startExperiment() {
    if (started == false) {
        $('#targetText').text(E_texts[num_E_complete])
        $('#targetText').css({'background-color': 'white'})
        $('#typedText').text('')
        started = true;
        startTime = new Date()

        $('#hidden').css({'position': 'fixed',
                            'left':'2%', 'width':'5%', 'padding':'0.1%', 'background-color':'white'})
        var $quit = $('<input type="button" value="QUIT WITHOUT COMPLETING" />');
        $quit.appendTo($("#hidden")); 
    }
}

function transformTypedChar(charStr) {
    console.log(switched_E[num_E_complete].length)
    if (switch_current == true) {

        if (switched_E[num_E_complete].length == 2) {
            switch (charStr) {
                case switched_E[num_E_complete][0].toLowerCase(): return switched_E[num_E_complete][1].toLowerCase();
                case switched_E[num_E_complete][1].toLowerCase(): return switched_E[num_E_complete][0].toLowerCase();
                case switched_E[num_E_complete][0].toUpperCase(): return switched_E[num_E_complete][1].toUpperCase();
                case switched_E[num_E_complete][1].toUpperCase(): return switched_E[num_E_complete][0].toUpperCase();
                //case "a": return "b";
                //case "b": return "a";
                //case "A": return "B";
                //case "B": return "A";
            }}

        else if (switched_E[num_E_complete].length == 4) {
            switch (charStr) {
                case switched_E[num_E_complete][0].toLowerCase(): return switched_E[num_E_complete][1].toLowerCase();
                case switched_E[num_E_complete][1].toLowerCase(): return switched_E[num_E_complete][0].toLowerCase();
                case switched_E[num_E_complete][0].toUpperCase(): return switched_E[num_E_complete][1].toUpperCase();
                case switched_E[num_E_complete][1].toUpperCase(): return switched_E[num_E_complete][0].toUpperCase();

                case switched_E[num_E_complete][2].toLowerCase(): return switched_E[num_E_complete][3].toLowerCase();
                case switched_E[num_E_complete][3].toLowerCase(): return switched_E[num_E_complete][2].toLowerCase();
                case switched_E[num_E_complete][2].toUpperCase(): return switched_E[num_E_complete][3].toUpperCase();
                case switched_E[num_E_complete][3].toUpperCase(): return switched_E[num_E_complete][2].toUpperCase();
            }}

        else if (switched_E[num_E_complete].length == 6) {
            switch (charStr) {
                case switched_E[num_E_complete][0].toLowerCase(): return switched_E[num_E_complete][1].toLowerCase();
                case switched_E[num_E_complete][1].toLowerCase(): return switched_E[num_E_complete][0].toLowerCase();
                case switched_E[num_E_complete][0].toUpperCase(): return switched_E[num_E_complete][1].toUpperCase();
                case switched_E[num_E_complete][1].toUpperCase(): return switched_E[num_E_complete][0].toUpperCase();

                case switched_E[num_E_complete][2].toLowerCase(): return switched_E[num_E_complete][3].toLowerCase();
                case switched_E[num_E_complete][3].toLowerCase(): return switched_E[num_E_complete][2].toLowerCase();
                case switched_E[num_E_complete][2].toUpperCase(): return switched_E[num_E_complete][3].toUpperCase();
                case switched_E[num_E_complete][3].toUpperCase(): return switched_E[num_E_complete][2].toUpperCase();

                case switched_E[num_E_complete][4].toLowerCase(): return switched_E[num_E_complete][5].toLowerCase();
                case switched_E[num_E_complete][5].toLowerCase(): return switched_E[num_E_complete][4].toLowerCase();
                case switched_E[num_E_complete][4].toUpperCase(): return switched_E[num_E_complete][5].toUpperCase();
                case switched_E[num_E_complete][5].toUpperCase(): return switched_E[num_E_complete][4].toUpperCase();
            }}
    }
    else {return charStr}
}

// MAIN FUNCTION BELOW
$(function() {
    $('#typedText').on('focus', startExperiment)
    // if (started == true) {document.getElementById('typedText').focus()}

    $('#typedText').on('keyup', function() {
        if (window.event.which == 8) {
            consoleArray.push(['[', 'BACKSPACE', passedTime+' ms', ']'] )
        }
        if (token2 == true) { // FIX CLEAR TEXTAREA
            $('#typedText').val('') // FIX CLEAR TEXTAREA
            token2 = false // FIX CLEAR TEXTAREA
        } // FIX CLEAR TEXTAREA
    })

    $('#typedText').on('keypress', function() {
        //textEntered = document.getElementById('typedText').value  // text already typed into textarea 

        if (token == true) {
            var sel = getInputSelection(this), val = this.value;
            this.value = val.slice(0, sel.start - 1) + val.slice(sel.start);
            token = false
        }

        lastkey = String.fromCharCode( window.event.which ) // charCode for the keyup event
        console.log(lastkey)
        // If new character is a switching character. //

        upperlastkey = lastkey.toUpperCase()
        
        //if (lastkey == 'a' || lastkey == 'b' || lastkey == 'A' || lastkey == 'B') {
        if (switched_E[num_E_complete].includes(upperlastkey)) {
            // from jsFiddle example
            //var charStr = textEntered.slice(-1) // only switch the last key
            var transformedChar = transformTypedChar(lastkey);
            var sel = getInputSelection(this), val = this.value;
            this.value = val.slice(0, sel.start) + transformedChar + val.slice(sel.end);
            // Move the caret
            setInputSelection(this, sel.start, sel.start);
            //return false;
            token = true
        }

        if (window.event.which == 46 || window.event.which == 32 || window.event.which == 63 ) { //32=spacebar, 46=period
            if (E_texts[num_E_complete].indexOf(this.value) == 0) {  // if text is accurate and no characters precede
                console.log('YAY'); 
                console.log(E_texts[num_E_complete]); 
                console.log(this.value);  //this.value is the currently entered text
                consoleArray.push( ['[', 'YAY', ']'] );  // , this.value, E_texts[num_E_complete]

                $('#typedText').css({'background-color': 'rgb(211,237,199)'})
                if (E_texts[num_E_complete].slice(0, -1) == this.value) { // IF EXAMPLE IS COMPLETE
                    console.log('COMPLETE!')
                    consoleArray.push( ['[','COMPLETE!', ']'] )
                    num_E_complete ++   // incrementally add 1 to num complete

                    if (num_E_complete == 5) {  // experiment complete
                        $('#targetText').css({'background-color': 'rgb(213, 255, 207)'})
                        //window.alert(consoleArray)
                        $('#hidden').css({'position': 'fixed',
                            'left':'2%', 'width':'20%', 'padding':'1%', 'background-color':'white'})
                        var $input = $('<input type="button" value="Copy data log" />');
                        $input.appendTo($("#hidden")); 
                        $('#hidden').html('<a href="https://forms.gle/dfUtzgUNf1yQgdCZ7">Click here, then paste from clipboard (Ctrl + v) into this Google form</a>');
                        
                        EXPERIMENT_COMPLETE = true;
                        
                        

                        // copy to clipboard
                        navigator.clipboard.writeText(consoleArray).then(function() {
                            window.alert("Copy successful! Paste into Google form.")
                            }, function() {
                                window.alert("copy failed")
                            });
                        }

                    else {
                        $('#targetText').text(E_texts[num_E_complete])
                        //$('#targetText').css({'background-color': 'white'})
                        token2 = true // FIX CLEAR TEXTAREA
                        

                    }

                    
                
                    
                }
            }
            else {console.log('YUCK'); console.log(E_texts[num_E_complete]); console.log(this.value);
                consoleArray.push( ['[', 'YUCK', ']'] );  // , this.value, E_texts[num_E_complete]
                $('#typedText').css({'background-color': 'rgb(250, 209, 205)'})}
        }
        passedTime = new Date() // gets time of keyup
        passedTime = passedTime - startTime // calculates ms
        // console.log(lastkey, passedTime+' ms')
        consoleArray.push(['[', lastkey, passedTime+' ms', ']'] ) // save to log variable

       

    })
    // Log copy function
    $( "#hidden" ).click(function() {
        if (EXPERIMENT_COMPLETE == true) {
        navigator.clipboard.writeText(consoleArray).then(function() {
            window.alert("Copy successful! Paste into Google form.")
            }, function() {
                window.alert("copy failed")
            });}
        if (EXPERIMENT_COMPLETE == false) {
            $('#hidden').html('<a href="https://forms.gle/dfUtzgUNf1yQgdCZ7">Click here, then paste from clipboard (Ctrl + v) into this Google form</a>');
            consoleArray.push("[QUIT EARLY]")
            navigator.clipboard.writeText(consoleArray).then(function() {
                window.alert("Copy successful! Paste into Google form.")
                }, function() {
                    window.alert("copy failed")
                });}
    });    
})
// MAIN FUNCTION ABOVE




function getInputSelection(el) {
    var start = 0, end = 0, normalizedValue, range,
        textInputRange, len, endRange;

    if (typeof el.selectionStart == "number" && typeof el.selectionEnd == "number") {
        start = el.selectionStart;
        end = el.selectionEnd;
    } else {
        range = document.selection.createRange();

        if (range && range.parentElement() == el) {
            len = el.value.length;
            normalizedValue = el.value.replace(/\r\n/g, "\n");

            // Create a working TextRange that lives only in the input
            textInputRange = el.createTextRange();
            textInputRange.moveToBookmark(range.getBookmark());

            // Check if the start and end of the selection are at the very end
            // of the input, since moveStart/moveEnd doesn't return what we want
            // in those cases
            endRange = el.createTextRange();
            endRange.collapse(false);

            if (textInputRange.compareEndPoints("StartToEnd", endRange) > -1) {
                start = end = len;
            } else {
                start = -textInputRange.moveStart("character", -len);
                start += normalizedValue.slice(0, start).split("\n").length - 1;

                if (textInputRange.compareEndPoints("EndToEnd", endRange) > -1) {
                    end = len;
                } else {
                    end = -textInputRange.moveEnd("character", -len);
                    end += normalizedValue.slice(0, end).split("\n").length - 1;
                }
            }
        }
    }

    return {
        start: start,
        end: end
    };
}

function offsetToRangeCharacterMove(el, offset) {
    return offset - (el.value.slice(0, offset).split("\r\n").length - 1);
}

function setInputSelection(el, startOffset, endOffset) {
    el.focus();
    if (typeof el.selectionStart == "number" && typeof el.selectionEnd == "number") {
        el.selectionStart = startOffset;
        el.selectionEnd = endOffset;
    } else {
        var range = el.createTextRange();
        var startCharMove = offsetToRangeCharacterMove(el, startOffset);
        range.collapse(true);
        if (startOffset == endOffset) {
            range.move("character", startCharMove);
        } else {
            range.moveEnd("character", offsetToRangeCharacterMove(el, endOffset));
            range.moveStart("character", startCharMove);
        }
        range.select();
    }
}
